import random
import threading

SEND_CHANCE = 80  # 80% chance to actually send (and not get lost)
CORRUPT_CHANCE = 20  # 20% chance to corrupt the frame given sending event

# Corrupt a binary frame by flipping a random bit
def corrupt(frame): 
    bit = random.randint(0, 15) #Generate a random bit position between 0 and 15
    return frame ^ (1 << bit) #Use XOR to flip the selected bit in the frame

# Simulate frame transmission and potential corruption or loss
def mightsend(frame):
    random_value = random.randint( 1 , 100 ) #Random number between 1 and 100
    if random_value <= SEND_CHANCE:  # Chance to send and not get lost on the way
        random_value = random.randint(1, 100) #Random number between 1 and 100
        if random_value <= CORRUPT_CHANCE:  # Chance to corrupt
            corrupted_frame = corrupt(frame) #Call the corrupt function to corrupt the frame
            # Case 4 : timeout
            print(f"[[ CHANNEL: The frame {frame} is CORRUPTED into frame {corrupted_frame} ]]") #Print a message indicating the frame was corrupted
            frame = corrupted_frame #Update the frame with the corrupted version
        # Case 1   : frame is successfully sent  
        print(f"[[ CHANNEL: The frame {frame} is SENT ]]")
    else:
        # Case 2 / 3 : frame is lost
        print(f"[[ CHANNEL: The frame {frame} is LOST ]]")

def main():
    frames = [random.randint(0, 65535) for _ in range(10)]  # Generate 10 random frames

    for frame in frames:
        t = threading.Thread(target=mightsend, args=(frame,)) #Create a now thread for each frame,Calling 'mightsend' function with the frame as an argument
        t.start()# Start the thread to simulate channel transmission

if __name__ == "__main__":
    main()


